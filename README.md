# Final Project Laravel batch 40

## 1. Informasi Kelompok

Nomor kelompok : 4

Anggota:
- Rahmat Rafli
- Iman Maulana
- Danang Ari Murti Wibowo

### Tema Project:
<b>"Sistem Informasi Perpustakaan"</b>

### ERD
![ERD Perpus](/public/ERD/erdperpus.png "ERD Perpus")

## 2. Link video demo
Role Guest: https://youtu.be/xPjMm05z-8g
Role Admin: https://youtu.be/ShSjdq6aiME

## 3. Link Deploy

<b>http://final-project.danangamw.sanbercodeapp.com/</b>
untuk login sebagai admin
email : admin@admin.com
password : admin1234
