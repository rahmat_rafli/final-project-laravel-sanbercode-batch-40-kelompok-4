<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Role::create([
            'nama' => 'admin',
        ]);

        Role::create([
            'nama' => 'guest',
        ]);

        $user = User::create([
            'username' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin1234'),
            'role_id' => 1,
        ]);

        Profile::create([
            'nama' => 'Yo ndak tau',
            'jenis_kelamin' => 'laki-laki',
            'alamat' => 'disini',
            'nomor_telepon' => 123456,
            'users_id' => $user->id
        ]);
    }
}
